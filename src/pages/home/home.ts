import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {  ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {TeamPage} from '../team/team';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	testes:any;

	constructor(public navCtrl: NavController,private http: Http) {

	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad HomePage');

		this.http.get("https://soccer.sportmonks.com/api/v2.0/livescores?api_token=re0tQXO6WTJrqjwdp4D6ib8ceWhDAjK2tR3FLVF7Oj9m2ZzxddyrVX0gaGHQ&include=localTeam,visitorTeam")
		.subscribe(testes => {
			this.testes = testes.json();
			this.testes = this.testes["data"];	
		});

	}

	pushPage(index){
		this.navCtrl.push(TeamPage, {
			id: index
		});
	}

}

 // In this page I show livescore with team name score ok? What I want : When I click on team => go to team page BUT
 /*
 hello I am here
 hi
 hi this?yes
 
 what is your problem?
 well
 As u can see for the moment I use this Api => give me a live score Each match have home team and away team so 2 id 1 for away 1 for home.

 What I want is : 

 When I clic on home name = like CSKA MOSKOVA => GO TO TEAM PAGE => lets go to team page
 */
